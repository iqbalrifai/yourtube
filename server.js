var express = require('express');
var app     = express();
var cors    = require('cors');
var downloader = require('./downloader');
var streamer   = require('./streamer');
var search     = require('./search');
var sendSeekable = require('send-seekable');
var youtubeStream = require('youtube-audio-stream')
var spotify = require('./spotify');
var byid = require('./byid');
require('dotenv').config()

app.set('json spaces', 2);
app.use(cors());
app.use(sendSeekable);

app.get('/', function(req, res){
  res.json({
    status: 'online',
    description: 'SEHAT MAS ?'
  });
});
app.get('/dl/:ytid', downloader);
app.get('/stream/:id', streamer);
app.get('/search', search);
app.get('/song/:id', byid);
app.get('/yt/:id', function(req, res){
  var requestUrl = 'http://youtube.com/watch?v=' + req.params.id
  try {
    youtubeStream(requestUrl).pipe(res)
  } catch (exception) {
    res.status(500).send(exception)
  }
})

app.get('/spotify_redirect', (req, res) => {
  const url = spotify.getAuthRedirect(req);
  res.redirect(url);
})
app.get('/spotify_callback', (req, res) => {
  const code = req.query.code;
  spotify.getTokens(req, code)
  .then(({data}) => {
    const url = `${process.env.FRONTEND}/callback`
    const redirection = spotify.makeUrl(url, data)
    res.redirect(redirection);
  })
  .catch(err => {
    console.log(err);
    res.status(500).send();
  })
})
app.get('/domain', (req, res) => {
  const proto = req.protocol
  const host = req.get('host')
  res.json({host, proto})
})

var server = app.listen(process.env.PORT || 3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('app listening at http://%s:%s', host, port);
});